﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace cs241IntroLib
{
    public class BasicFunctions
    {
        // In C#, static functions are member functions that can be
        // called without an instance of the class object.  To invoke
        // a static function we use the name of the class followed by
        // a dot (.) followed by the name of the function.
        // To call this function to add a + b we would use something like:
        // BasicFunctions.Cs241Add(a, b);
        public static int CS241Add(int first, int second)
        {
            return first + second;
        }

        public static int readInt()
        {
            string line = Console.ReadLine();
            int intRead = Int32.Parse(line);
            return intRead;
        }
    }
}
