﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using cs241IntroLib;


namespace cs241IntroToCSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter an integer: ");
            int x = BasicFunctions.readInt();

            Console.Write("Enter an integer: ");
            int y = BasicFunctions.readInt();
            int sum = BasicFunctions.CS241Add(x, y);
            Console.WriteLine("{0} + {1} = {2}", x, y, sum);
            Console.WriteLine("Hit return to continue...");
            Console.ReadLine();
        }
    }
}
