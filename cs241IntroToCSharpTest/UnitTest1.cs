﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using cs241IntroLib;

namespace cs241IntroToCSharpTest
{
    // Refer to Sedgewick's Algorithms (Fourth Edition) Chapter 1.
    // This project addresses the Basic Programming Model for C#

    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void BooleanTest()
        {
            // C# has a Boolean data type - the only possible values are 
            // true and false.
            bool itIsRaining = true;
            bool itIsSnowing = false;

            // When we test a function, we use the Assert class to 
            // see if the function produced the desired result.
            // Thing to Try:  see what happens if we change aTrueThing to false.
            // aTrueThing = false;
            Assert.IsTrue(itIsRaining);
            Assert.IsFalse(itIsSnowing);
        }


        [TestMethod]
        public void CS241SimpleAddTest()
        {
            // Test a function that adds two numbers

            int firstOperand = 10;
            int secondOperand = 100;

            // Call the function and record the results
            int sum = Add(firstOperand, secondOperand);

            // See if the results are what we expected
            // When calling Assert.AreEqual the expected value is the first
            // parameter, and the computed value is the second parameter.

            // Thing to Try: change the Add operator so that it generates 
            // an incorrect result.  See what happens.
            Assert.AreEqual(firstOperand + secondOperand, sum);
        }

        [TestMethod]
        public void dataTypeTest()
        {
            // These C# data types are mostly just like their C++ counterparts.
            // An exception is the char data type.
            // In C++ the char data type is one byte, but in C# it is two bytes.

            // Declare a 32-bit integer
            // Although it is legal to make more than one declaration per line,
            // a better style is to limit declarations to one per line.
            // This style will be required in this class.

            // Declare a signed integer.
            // int can store integers in the range 
            // -2,147,483,648 to 2,147,483,647
            int myInt = 10;
            Assert.AreEqual(4, sizeof(int));

            myInt = int.MaxValue;
            Assert.AreEqual(2147483647, myInt);

            // Declare a signed long integer (64-bit)
            // long can store integers in the range 
            // -9,223,372,036,854,775,808 to 9,223,372,036,854,775,807
            // approx 9.233 * 10^18
            long myLong = 10L;
            Assert.AreEqual(8, sizeof(long));
            myLong = long.MinValue;
            Assert.AreEqual(-9223372036854775808L, myLong);

            // Unsigned int (uint) can store integers in the range (32-bit)
            // 0 to 4,294,967,295
            uint myUint = 10U;
            Assert.AreEqual(4, sizeof(uint));
            myUint = uint.Parse("100");
            Assert.AreEqual(100U, myUint);

            // unsigned long (ulong) can store integers in the range (64-bit)
            // 0 to 18,446,744,073,709,551,615
            ulong myUlong = 10UL;
            Assert.AreEqual(8, sizeof(ulong));
            myUlong = ulong.MinValue;

            // Declare a double-precision floating-point value (64-bits)
            double myDouble = 3.14;
            Assert.AreEqual(8, sizeof(double));

            // Declare a single-precision floating-point value (32-bits)
            float myFloat = 3.14f;
            Assert.AreEqual(4, sizeof(float));

            // Declare a 16-bit char 
            // char can store a Unicode character.
            char myChar = 'A';
            Assert.AreEqual(2, sizeof(char));

            // Declare an 8-bit byte
            byte myByte = 65;
            Assert.AreEqual(1, sizeof(byte));

        }

        [TestMethod]
        public void castingTest()
        {
            // Type conversion in C# is accomplished using a "cast."
            // Casts in C# look the same as Java casts described in the text.

            // Convert 
            // Create a floating point variable.
            float myFloat = 3.14f;

            // The cast (int) in the statement below reads the value from 
            // myFloat and changes that value to an integer.  The integer
            // value is stored in myInt.  The conversion to int is done 
            // through truncation rather than rounding, so the fractional
            // part of myFloat is simply discarded.
            // myFloat is unaffected by the cast.
            int myInt = (int)myFloat;

            Assert.AreEqual(3, myInt);
            Assert.AreEqual(3.14f, myFloat);

            myFloat = 4.9999f;
            myInt = (int)myFloat;
            Assert.AreEqual(4.0f, myInt);

            // Unlike in C++, C# does not allow implicit casting when
            // data might be lost.
            // In C# it is illegal to write:
            // myInt = myFloat;
            // You must write
            // myInt = (int) myFloat;

            // However, lossless implicit casts are legal.
            myFloat = myInt;
            Assert.AreEqual(4.0f, myFloat);
        }

        [TestMethod]
        public void comparisonTest()
        {
            int small = 10;
            int alsoSmall = small;
            int large = 30;

            Assert.IsTrue(small < large);
            Assert.IsTrue(small == alsoSmall);
            Assert.IsTrue(small <= large);
            Assert.IsTrue(small <= alsoSmall);

            Assert.IsTrue(large > small);
            Assert.IsTrue(large >= small);
            Assert.IsTrue(small >= alsoSmall);

            Assert.IsTrue(large != small);
        }

        [TestMethod]
        public void compoundComparisonTest()
        {
            // Conjunction (AND) (&& operator) 
            // True when both operands are true.

            // False conjunctions
            Assert.IsFalse(true && false);
            Assert.IsFalse(false && true);
            Assert.IsFalse(false && false);

            // True conjunctions
            Assert.IsTrue(true && true);

            // Disjunction (Inclusive OR) (|| operator) 
            // True when any operand is true

            // False disjunctions
            Assert.IsFalse(false || false);

            // True disjunctions
            Assert.IsTrue(true || false);
            Assert.IsTrue(false || true);
            Assert.IsTrue(true || true);

            // Exclusive OR (^ operator)
            // True when exactly one two operands is true.

            // True Exclusive OR's
            Assert.IsTrue(false ^ true);
            Assert.IsTrue(true ^ false);

            // False Exclusive OR's
            Assert.IsFalse(false ^ false);
            Assert.IsFalse(true ^ true);
        }

        [TestMethod]
        public void simpleIfCondition()
        {
            Random random = new Random();

            int a = random.Next();
            int b = random.Next();

            // Test to see if a is less than b

            // Evaluate the condition a < b
            if(a < b)
            {
                // If the condition is true, execute this block of code.
                Assert.IsTrue(a < b);
            }
            else
            {
                // If it is false, execute the "else" block of code
                Assert.IsFalse(a < b);
            }
        }

        [TestMethod]
        public void simpleLoop()
        {
            const int N = 1024;

            // Sum all the numbers from 1 to N
            // Just like C++!
            int sum = 0;
            for(int i = 1; i<= N; ++i)
            {
                sum = sum + i;
            }
            Assert.AreEqual((int)Math.Round(( (N+1) / 2.0) * N), sum);
        }

        [TestMethod]
        public void simpleArray()
        {
            int N_ELEMENTS_IN_ARRAY = 10;

            // This is not like C++!
            // This will not work -- 
            // int myArray[N_ELEMENTS_IN_ARRAY];

            // All arrays are dynamically allocated in C#
            // The syntax is different for dynamically allocated arrays in C#
            int[] myArray = new int[N_ELEMENTS_IN_ARRAY];

            // Also we never have to call delete!!!!!!!

            // Fill the array with integers
            // This looks like C++
            for (int i = 0; i < N_ELEMENTS_IN_ARRAY; i++)
            {
                myArray[i] = i;
            }

            // Add all the elements in the array
            int sum = 0;
            foreach (int x in myArray)
            {
                sum += x;
            }
            Assert.AreEqual((int)(Math.Round( ((N_ELEMENTS_IN_ARRAY-1) / 2.0) 
                * N_ELEMENTS_IN_ARRAY)), sum);
        }

        [TestMethod]
        public void IntegerAddTest()
        {

            // Generate a test case - it doesn't need to test a lot of 
            //  possibilities.  Generally, we will want to test just one thing 
            // per test.
            const int FIRST_OPERAND = 10;
            const int SECOND_OPERAND = 100;
            const int SUM = FIRST_OPERAND + SECOND_OPERAND;

            // Compute a result
            int sum = FIRST_OPERAND + SECOND_OPERAND;

            // See if the results are what we expected

            // Thing to Try: change the Add operator to subtractions.
            // See what happens.
            Assert.AreEqual(SUM, sum);
        }

        [TestMethod]
        public void BasicFunctionsAddTest()
        {
            // Test a function that adds two numbers.  This time the function
            // is a static function in another class.  The class is 
            // a class I made for demonstration called "BasicFunctions."
            // The definition of that function is in cs241IntroLib
            int firstOperand = 10;
            int secondOperand = 100;
            int sum = BasicFunctions.CS241Add(firstOperand, secondOperand);
            Assert.AreEqual(firstOperand + secondOperand, sum);
        }
        

        // Here Add() is a simple member function.
        // Usually we don't test functions inside a test class, but
        // I just wanted to start with the simplest possible example.
        int Add(int firstOp, int secondOp)
        {
            return firstOp + secondOp;
        }

    }
}
